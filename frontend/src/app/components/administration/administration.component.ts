import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { ContractorsService } from '../../services/contractors.service';
import { formatDate } from '@angular/common';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css'],
})
export class AdministrationComponent implements OnInit {
  contractForm: FormGroup = new FormGroup({
    contractorId: new FormControl('', [Validators.required]),
    employees: new FormControl([], [Validators.required]),
    hours: new FormControl('', [Validators.required]),
    startDate: new FormControl(
      formatDate(new Date(Date.now()), 'yyyy-MM-dd', 'en'),
      [Validators.required]
    ),
    job: new FormControl('', [Validators.required]),
  });

  contracts = [];
  contractors = [];
  employees = [];

  showContracts = [];

  private contractorLoaded = false;

  constructor(
    public employeesService: EmployeesService,
    public contractorsService: ContractorsService
  ) {}

  ngOnInit(): void {
    this.getEmployees();
    this.getContractors();
    this.getContract();
  }

  onDrop(event: CdkDragDrop<string[]>) {
    const isEmployee = !event.previousContainer.data[0]['company'];
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else if (!this.contractorLoaded || isEmployee) {
      this.contractorLoaded = this.contractorLoaded
        ? this.contractorLoaded
        : !!event.previousContainer.data[0]['company'];

      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    this.fillContractInfo();
  }

  fillContractInfo() {
    let tempEmployees = [];
    this.contractForm.controls.employees.setValue([]);
    this.contracts.map((contract) => {
      const isEmployee = !contract['company'];

      if (isEmployee) {
        tempEmployees.push({
          employeeId: contract._id,
          employeeName: contract.name,
        });
      } else {
        this.contractForm.controls.contractorId.setValue(contract._id);
      }
    });
    this.contractForm.controls.employees.setValue(tempEmployees);
  }

  disassociate(deleteContract) {
    this.contracts.map((contract, index) => {
      if (deleteContract == contract) {
        this.contracts.splice(index, 1);
      }
    });

    const isEmployee = !deleteContract['company'];
    if (isEmployee) {
      this.employees.push(deleteContract);
    } else {
      this.contractors.push(deleteContract);
      this.contractorLoaded = false;
    }
  }

  saveContract(form: FormGroup) {
    this.contractorsService
      .addContract(form.controls.contractorId.value, form.value)
      .subscribe((res) => {
        this.clearContract(form);
        this.getContract();
      });
  }

  clearContract(contractForm) {
    this.contractorLoaded = false;
    this.contracts = [];
    this.getEmployees();
    this.getContractors();
    contractForm.reset();
  }

  getContract() {
    this.contractorsService.getContracts().subscribe(
      (res) => {
        this.showContracts = res['contracts'];
        console.log(this.showContracts);
      },
      (err) => console.error(err)
    );
  }

  getEmployees() {
    this.employeesService.getEmployees().subscribe(
      (res) => {
        this.employees = res['employees'];
      },
      (err) => console.error(err)
    );
  }

  getContractors() {
    this.contractorsService.getContractors().subscribe(
      (res) => {
        this.contractors = res['contractor'];
      },
      (err) => console.error(err)
    );
  }

  missingHours(hours, startDate, flag) {
    let currentDate = new Date(
      Date.parse(formatDate(new Date(Date.now()), 'yyyy-MM-dd', 'en')) +
        18000000 +
        86400000
    );
    let tmpStartDate = new Date(Date.parse(startDate) + 18000000);
    let days =
      (tmpStartDate.getTime() - currentDate.getTime()) / (1000 * 3600 * 24);
    const pendingHours = hours + days * 8;
    if (flag) {
      if (tmpStartDate.getTime() - currentDate.getTime() < 0) {
        return pendingHours > 0 ? -days * 8 : 0;
      } else {
        return 0;
      }
    } else {
      if (tmpStartDate.getTime() - currentDate.getTime() < 0) {
        return pendingHours > 0 ? hours + days * 8 : 0;
      } else {
        return hours;
      }
    }
  }
}
