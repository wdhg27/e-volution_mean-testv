import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Employees } from '../../models/employees';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
})
export class EmployeesComponent implements OnInit {
  employeeForm: FormGroup;
  id: string;
  updateForm: boolean = false;
  constructor(public employeesService: EmployeesService) {}

  ngOnInit(): void {
    this.getEmployees();
    this.initialForm('', '', '', '', '', '', '', '');
  }

  initialForm(
    name: string,
    lastName: string,
    id: string,
    typeOfContract: string,
    birthdate: string,
    address: string,
    phone: string,
    email: string
  ) {
    this.employeeForm = new FormGroup({
      name: new FormControl(name, [Validators.required]),
      lastName: new FormControl(lastName, [Validators.required]),
      id: new FormControl(id, [Validators.required]),
      typeOfContract: new FormControl(typeOfContract, [Validators.required]),
      birthdate: new FormControl(birthdate, [Validators.required]),
      address: new FormControl(address, [Validators.required]),
      phone: new FormControl(phone, [Validators.required]),
      email: new FormControl(email, [Validators.required]),
    });
  }

  getEmployees() {
    this.employeesService.getEmployees().subscribe(
      (res) => {
        this.employeesService.employees = res['employees'];
      },
      (err) => console.error(err)
    );
  }

  addEmployee(form: FormGroup) {
    if (this.updateForm) {
      this.employeesService.putEmployee(this.id, form.value).subscribe(
        (res) => {
          form.reset();
          this.getEmployees();
          this.updateForm = false;
        },
        (err) => console.log(err)
      );
    } else {
      this.employeesService.createEmployee(form.value).subscribe(
        (res) => {
          this.getEmployees();
          form.reset();
        },
        (err) => console.log(err)
      );
    }
  }

  updateForms(updateForm: boolean) {
    this.updateForm = updateForm;
    console.log(this.updateForm);
  }

  editEmployee(employee: Employees) {
    this.id = employee._id;
    this.initialForm(
      employee.name,
      employee.lastName,
      employee.id,
      employee.typeOfContract,
      employee.birthdate,
      employee.address,
      employee.phone,
      employee.email
    );
  }

  deleteEmployee(employee: Employees) {
    if (confirm('are you sure?')) {
      this.employeesService.deleteEmployee(employee._id).subscribe(
        (res) => {
          this.getEmployees();
        },
        (err) => console.log(err)
      );
    }
  }
}
