import { Component, OnInit } from '@angular/core';
import { ContractorsService } from '../../services/contractors.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Contractors } from '../../models/contractors';

@Component({
  selector: 'app-contractors',
  templateUrl: './contractors.component.html',
  styleUrls: ['./contractors.component.css'],
})
export class ContractorsComponent implements OnInit {
  contractorForm: FormGroup;
  id: string;
  updateForm: boolean = false;

  constructor(public contractorsService: ContractorsService) {}

  ngOnInit(): void {
    this.getContractos();
    this.initialForm('', '', '', '', '', '', '', '');
  }

  initialForm(
    name: string,
    lastName: string,
    id: string,
    birthdate: string,
    address: string,
    phone: string,
    email: string,
    company: string
  ) {
    this.contractorForm = new FormGroup({
      name: new FormControl(name, [Validators.required]),
      lastName: new FormControl(lastName, [Validators.required]),
      id: new FormControl(id, [Validators.required]),
      birthdate: new FormControl(birthdate, [Validators.required]),
      address: new FormControl(address, [Validators.required]),
      phone: new FormControl(phone, [Validators.required]),
      email: new FormControl(email, [Validators.required]),
      company: new FormControl(company, [Validators.required]),
    });
  }

  getContractos() {
    this.contractorsService.getContractors().subscribe(
      (res) => {
        this.contractorsService.contractors = res['contractor'];
      },
      (err) => console.error(err)
    );
  }

  addContractor(form: FormGroup) {
    if (this.updateForm) {
      this.contractorsService.putContractor(this.id, form.value).subscribe(
        (res) => {
          form.reset();
          this.getContractos();
          this.updateForm = false;
        },
        (err) => console.log(err)
      );
    } else {
      this.contractorsService.createContractor(form.value).subscribe(
        (res) => {
          this.getContractos();
          form.reset();
        },
        (err) => console.log(err)
      );
    }
  }

  updateForms(updateForm: boolean) {
    this.updateForm = updateForm;
  }

  editContractor(contractor: Contractors) {
    this.id = contractor._id;
    this.initialForm(
      contractor.name,
      contractor.lastName,
      contractor.id,
      contractor.birthdate,
      contractor.address,
      contractor.phone,
      contractor.email,
      contractor.company
    );
  }

  deleteContractor(contractor: Contractors) {
    if (confirm('are you sure?')) {
      this.contractorsService.deleteContractor(contractor._id).subscribe(
        (res) => {
          this.getContractos();
        },
        (err) => console.log(err)
      );
    }
  }
}
