export interface Contractors {
  name: string;
  lastName: string;
  id: string;
  birthdate: string;
  address: string;
  phone: string;
  email: string;
  company: string;
  createdAt?: string;
  _id?: string;
}
