export interface Employees {
  name: string;
  lastName: string;
  id: string;
  typeOfContract: string;
  birthdate: string;
  address: string;
  phone: string;
  email: string;
  createdAt?: string;
  _id?: string;
}
