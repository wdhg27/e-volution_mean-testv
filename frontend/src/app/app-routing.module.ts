import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { EmployeesComponent } from './components/employees/employees.component';
import { ContractorsComponent } from './components/contractors/contractors.component';
import { AdministrationComponent } from './components/administration/administration.component';

const routes: Routes = [
  { path: 'api/employees', component: EmployeesComponent },
  { path: 'api/contractors', component: ContractorsComponent },
  { path: 'administration', component: AdministrationComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
