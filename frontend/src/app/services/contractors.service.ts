import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contractors } from '../models/contractors';

@Injectable({
  providedIn: 'root',
})
export class ContractorsService {
  URL_API = 'http://localhost:4000/api/contractors';

  contractors: Contractors[];

  constructor(private http: HttpClient) {}

  getContractors() {
    return this.http.get<Contractors[]>(`${this.URL_API}/getContractors`);
  }

  createContractor(contractors: Contractors) {
    return this.http.post(`${this.URL_API}/createContractor`, contractors);
  }

  addContract(id: string, contract: any) {
    return this.http.post(`${this.URL_API}/addContract/${id}`, contract);
  }

  getContracts() {
    return this.http.get(`${this.URL_API}/getContracts`);
  }

  putContractor(id: string, contractors: Contractors) {
    return this.http.put(`${this.URL_API}/editContractor/${id}`, contractors);
  }

  deleteContractor(_id: string) {
    return this.http.delete(`${this.URL_API}/deleteContractor/${_id}`);
  }
}
