import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employees } from '../models/employees';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  URL_API = 'http://localhost:4000/api/employees';

  employees: Employees[];

  selectedEmployee: Employees = {
    name: '',
    lastName: '',
    id: '',
    typeOfContract: '',
    birthdate: '',
    address: '',
    phone: '',
    email: '',
  };

  constructor(private http: HttpClient) {}

  getEmployees() {
    return this.http.get<Employees[]>(`${this.URL_API}/getEmployees`);
  }

  createEmployee(employees: Employees) {
    return this.http.post(`${this.URL_API}/createEmployee`, employees);
  }

  putEmployee(id: string, employees: Employees) {
    return this.http.put(`${this.URL_API}/editEmployee/${id}`, employees);
  }

  deleteEmployee(_id: string) {
    return this.http.delete(`${this.URL_API}/deleteEmployee/${_id}`);
  }
}
