const { Router } = require ('express')
const router = Router()

const employeesCtrl = require ('../controllers/employees.controller')

router.get('/getEmployees', employeesCtrl.getEmployees )

router.post('/createEmployee', employeesCtrl.createEmployee )

router.get('/getEmployee/:id', employeesCtrl.getEmployee )

router.put('/editEmployee/:id', employeesCtrl.editEmployee )

router.delete('/deleteEmployee/:id', employeesCtrl.deleteEmployee )


module.exports = router