const { Router } = require ('express')
const router = Router()

const contractorCtrl = require ('../controllers/contractors.controller')

router.get('/getContractors', contractorCtrl.getContractors )

router.get('/getContracts', contractorCtrl.getContracts )

router.post('/createContractor', contractorCtrl.createContractor )

router.post('/addContract/:id', contractorCtrl.addContract )

router.get('/getContractor/:id', contractorCtrl.getContractor )

router.put('/editContractor/:id', contractorCtrl.editContractor )

router.delete('/deleteContractor/:id', contractorCtrl.deleteContractor )


module.exports = router