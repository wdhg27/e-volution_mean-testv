const express = require ('express');
const morgan = require ('morgan');
const cors = require ('cors');

const app= express();

app.set('port', process.env.PORT || 4000);

app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
// used to understand date from HTML forms
app.use(express.urlencoded({ extended: false }))

// routes
app.use("/api/employees", require('./routes/employees.routes'));
app.use("/api/contractors", require('./routes/contractors.routes'));

module.exports = app;