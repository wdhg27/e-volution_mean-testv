const { Schema, model } = require('mongoose')

const employeeSchema = new Schema({
    name: {type: String, required: true},
    lastName: {type: String, required: true},
    id: {type: String, required: true, unique: true},
    typeOfContract: {type: String, required: true},
    birthdate: {type: String, required: true},
    address: {type: String, required: true},
    phone: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    },{
        timestamps: true,
        versionKey: false
    });

    module.exports = model("Employee", employeeSchema)