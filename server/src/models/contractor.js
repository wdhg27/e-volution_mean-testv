const { Schema, model } = require('mongoose')

const contractorSchema = new Schema({
    name: {type: String, required: true},
    lastName: {type: String, required: true},
    id: {type: String, required: true, unique: true},
    birthdate: {type: String, required: true},
    address: {type: String, required: true},
    phone: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    company: {type: String, required: true},
    contracts: {type: Array, required: false},
    },{
        timestamps: true,
        versionKey: false
    });

    module.exports = model("Contractor", contractorSchema)