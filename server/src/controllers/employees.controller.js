const employeeCtrl = {};

const Employee = require ('../models/employee');

employeeCtrl.getEmployees = async (req, res) => {
   try{
      const employees = await Employee.find();
      if(!employees.length) {
         return res.status(400).json({
            ok: false,
            massage: 'there are not employees',
         });
      }
      return res.status(200).json({
         ok: true,
         employees: employees,
      });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         error: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
};


employeeCtrl.createEmployee = async (req, res) => {
   try {
      const employeeExist = await Employee.findOne({
      $or: [
         { email: req.body.email },
         { id: req.body.id }
      ]
   });

   if(!employeeExist) {
      const _employee = new Employee(req.body);
      await _employee.save();

      if(!_employee){
         return res.status(400).json({
            ok:false,
            message:'The user could not be created.'
         });
      }
      return res.status(201).json({
         ok:true,
         employee: _employee,
      });
   } else {
      return res.status(400).json({
         ok:false,
         message: 'The user with that email / id already exists.'
      });
   }
   } catch (error) {
      return res.status(500).json({
         ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      });
   }
};


employeeCtrl.getEmployee = async (req, res) => {
try {
   const employee = await Employee.findById (req.params.id);
   if(!employee) {
      return res.status(400).json({
         ok:false,
         message: 'The user was not found in the application',
      });
   } return res.status(200).json({
      ok: true,
      employee: employee,
   });
} catch (error) {
   return res.status(500).json({
      ok:false,
      message: 'Internal server error, this operation could not be performed.',
      error,
   });
}
};


employeeCtrl.editEmployee = async (req, res) => {
   try{
      const employee = await Employee.findByIdAndUpdate(req.params.id, req.body);
      if(!employee) {
         return res.status (400).json({
            ok: false,
            message: 'The user was not found in the application',
         });
      }
         return res.status(200).json({
            ok: true,
            message: 'employee Updated',
         });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
}

employeeCtrl.deleteEmployee = async (req, res) => {
   try {
      const deletedEmployee = await Employee.findByIdAndDelete(req.params.id);
      if(!deletedEmployee){
         return res.status (400).json({
            ok: false,
            message: 'The user was not found in the application',
      });
      }
      return res.status(200).json({
         ok: true,
         message: 'employee deleted',
      });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
};


module.exports = employeeCtrl;