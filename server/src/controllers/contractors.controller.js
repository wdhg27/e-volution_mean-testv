const contractorCtrl = {};

const Contractor = require ('../models/contractor');

contractorCtrl.getContractors = async (req, res) => {
   try{
      const contractor = await Contractor.find();
      if(!contractor.length) {
         return res.status(400).json({
               ok: false,
               massage: 'there are not contractor',
         });
      }
      return res.status(200).json({
         ok: true,
         contractor: contractor,
      });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         error: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
};


contractorCtrl.createContractor = async (req, res) => {
   try {
      const contractorExist = await Contractor.findOne({
      $or: [
         { email: req.body.email },
         { id: req.body.id }
      ]
   });

   if(!contractorExist) {
      const _contractor = new Contractor(req.body);
      await _contractor.save();

   if(!_contractor){
         return res.status(400).json({
               ok:false,
               message:'The user could not be created.'
         });
      }
      return res.status(201).json({
         ok:true,
         contractor: _contractor,
      });
   } else {
      return res.status(400).json({
         ok:false,
         message: 'The user with that email / id already exists.'
      });
   }
   } catch (error) {
      return res.status(500).json({
      ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      });
   }
};


contractorCtrl.getContractor = async (req, res) => {
try {
   const contractor = await Contractor.findById (req.params.id);
   if(!contractor) {
      return res.status(400).json({
         ok:false,
         message: 'The user was not found in the application',
      });
   } return res.status(200).json({
      ok: true,
      contractor: contractor,
   });
} catch (error) {
   return res.status(500).json({
      ok:false,
      message: 'Internal server error, this operation could not be performed.',
      error,
   });
}
};


contractorCtrl.editContractor = async (req, res) => {
   try{
      const contractor = await Contractor.findByIdAndUpdate(req.params.id, req.body);
      if(!contractor) {
         return res.status (400).json({
            ok: false,
            message: 'The user was not found in the application',
         });
      }
         return res.status(200).json({
            ok: true,
            message: 'contractor Updated',
         });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
}


// contracts: [
//    {employeeId: '1111',
//    employeeName: '111',
//    hours: 2,
//    startDate: '111',
//    job: '111'}
// ]
contractorCtrl.addContract = async (req, res) => {
   try{
      delete req.body.contractorId;
      const contractor = await Contractor.findById(req.params.id);
      if(!contractor) {
         return res.status (400).json({
            ok: false,
            message: 'The user was not found in the application',
         });
      }

      contractor.contracts.push(req.body)

      const modifiedContractor = await Contractor.findByIdAndUpdate(req.params.id, {contracts: contractor.contracts}, { new: true });
      console.log(modifiedContractor);
      return res.status(200).json({
         ok: true,
         message: 'New contract created',
      });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
}

contractorCtrl.getContracts = async (req, res) => {
   try{
      const contractors = await Contractor.find();
      if(!contractors.length) {
         return res.status(400).json({
               ok: false,
               massage: 'there are not contractor',
         });
      }
      let contracts = [];
      contractors.map((contractor) => {
         contractor.contracts.map((contract) => {
            console.log(contract);
            contracts.push({contractorName: contractor.name, contract})
         })
      })
      return res.status(200).json({
         ok: true,
         contracts: contracts,
      });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         error: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
}


contractorCtrl.deleteContractor = async (req, res) => {
   try {
      const deletedContractor = await Contractor.findByIdAndDelete(req.params.id);
      if(!deletedContractor){
         return res.status (400).json({
            ok: false,
            message: 'The user was not found in the application',
      });
      }
      return res.status(200).json({
         ok: true,
         message: 'contractor deleted',
      });
   } catch (error) {
      return res.status(500).json({
         ok:false,
         message: 'Internal server error, this operation could not be performed.',
         error,
      })
   }
};


module.exports = contractorCtrl;