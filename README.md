# Frontend

- Se implementó angular como framework JavaScript y bootstrap como framework de diseño.
- Para la funcionalidad de Drag and Drop de utilizó el cdk/drag-drop de angular.
- La aplicacion es full responsive (mobile first).
- Para poner a correr el proyecto parece sobre el folder y ejecute **cd fronted** y **ng serve**.

## Back (server)

- Se implementó Node.js y express.
- Para poner a correr el back parece sobre el folder y ejecute **cd server** y **npm run dev**.
- La base de datos se encuenta alojada en Mongo Atlas y la URL de conexion es mongodb+srv://mean:mean@cluster0.aaeyu.mongodb.net/mean?retryWrites=true&w=majority
  por lo tanto no es necesario intalar mongo localmente para ejecutar el proyecto.
